package ru.mtumanov.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public final class ProjectRemoveByIdRs extends AbstractProjectRs {

    public ProjectRemoveByIdRs(@Nullable final ProjectDTO project) {
        super(project);
    }

    public ProjectRemoveByIdRs(@Nullable final Throwable err) {
        super(err);
    }

}