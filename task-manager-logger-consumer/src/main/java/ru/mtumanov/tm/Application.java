package ru.mtumanov.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.mtumanov.tm.component.Bootstrap;
import ru.mtumanov.tm.configuration.LoggerConfiguration;

public class Application {

    public static void main(String[] args) {
        @NotNull final ConfigurableApplicationContext context = new AnnotationConfigApplicationContext(LoggerConfiguration.class);
        context.registerShutdownHook();
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.start();
    }

}
