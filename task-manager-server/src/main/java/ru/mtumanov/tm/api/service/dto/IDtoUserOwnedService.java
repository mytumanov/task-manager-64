package ru.mtumanov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.mtumanov.tm.enumerated.EntitySort;
import ru.mtumanov.tm.exception.AbstractException;

import java.util.List;

public interface IDtoUserOwnedService<M extends AbstractUserOwnedModelDTO> extends IDtoService<M> {

    @NotNull
    List<M> findAll(@NotNull String userId);

    @NotNull
    M add(@NotNull String userId, @NotNull M model) throws AbstractException;

    @NotNull
    List<M> findAll(@NotNull String userId, @Nullable EntitySort sort);

    @NotNull
    M findOneById(@NotNull String userId, @NotNull String id);

    @NotNull
    void remove(@NotNull String userId, @NotNull M model);

    @NotNull
    void removeById(@NotNull String userId, @NotNull String id);

    void clear(@NotNull String userId);

    long getSize(@NotNull String userId);

    boolean existById(@NotNull String userId, @NotNull String id);

}
