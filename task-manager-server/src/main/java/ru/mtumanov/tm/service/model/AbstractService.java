package ru.mtumanov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.mtumanov.tm.api.repository.model.IRepository;
import ru.mtumanov.tm.api.service.model.IService;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    @Autowired
    protected R repository;

    @Override
    public @NotNull
    List<M> findAll() {
        return repository.findAll();
    }

    @Override
    @NotNull
    public void set(@NotNull Collection<M> models) throws AbstractException {
        repository.saveAll(models);
    }

}
