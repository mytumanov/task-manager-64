package ru.mtumanov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.api.repository.model.ITaskRepository;
import ru.mtumanov.tm.api.service.model.ITaskService;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.entity.EntityNotFoundException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.exception.field.NameEmptyException;
import ru.mtumanov.tm.exception.user.UserIdEmptyException;
import ru.mtumanov.tm.model.Task;
import ru.mtumanov.tm.model.User;

import javax.transaction.Transactional;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    @Override
    @NotNull
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) throws AbstractException {
        if (projectId.isEmpty())
            return Collections.emptyList();
        if (userId.isEmpty())
            throw new UserIdEmptyException();

        return repository.findAllByUserIdAndProjectId(userId, projectId);
    }

    @Override
    @NotNull
    @Transactional
    public Task create(@NotNull String userId, @NotNull final String name, @NotNull final String description) throws AbstractException {
        if (userId.isEmpty())
            throw new UserIdEmptyException();
        if (name.isEmpty())
            throw new NameEmptyException();

        @NotNull Optional<User> userOptional = getUserRepository().findById(userId);
        if (!userOptional.isPresent())
            throw new EntityNotFoundException("Пользователь не найден id: " + userId);
        @NotNull final User user = userOptional.get();
        @NotNull final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUser(user);

        repository.save(task);
        return task;
    }

    @Override
    @NotNull
    @Transactional
    public Task updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        if (id.isEmpty())
            throw new IdEmptyException();
        if (userId.isEmpty())
            throw new UserIdEmptyException();
        if (name.isEmpty())
            throw new NameEmptyException();

        @NotNull final Task task = repository.findByUserIdAndId(userId, id);
        task.setName(name);
        task.setDescription(description);
        repository.save(task);
        return task;
    }

    @Override
    @NotNull
    @Transactional
    public Task changeTaskStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) throws AbstractException {
        if (id.isEmpty())
            throw new IdEmptyException();
        if (userId.isEmpty())
            throw new UserIdEmptyException();

        @NotNull final Task task = repository.findByUserIdAndId(userId, id);
        task.setStatus(status);
        repository.save(task);
        return task;
    }

}
