package ru.mtumanov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.mtumanov.tm.api.repository.dto.IDtoProjectRepository;
import ru.mtumanov.tm.api.repository.dto.IDtoTaskRepository;
import ru.mtumanov.tm.api.service.dto.IDtoProjectTaskService;
import ru.mtumanov.tm.dto.model.TaskDTO;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.entity.ProjectNotFoundException;
import ru.mtumanov.tm.exception.field.IdEmptyException;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class DtoProjectTaskService implements IDtoProjectTaskService {

    @NotNull
    @Autowired
    protected IDtoTaskRepository taskRepository;

    @NotNull
    @Autowired
    protected IDtoProjectRepository projectRepository;

    @Override
    @Transactional
    public void bindTaskToProject(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) throws AbstractException {
        if (projectId.isEmpty())
            throw new IdEmptyException();
        if (taskId.isEmpty())
            throw new IdEmptyException();
        if (!projectRepository.existsByUserIdAndId(userId, projectId))
            throw new ProjectNotFoundException();

        @NotNull final TaskDTO task = taskRepository.findByUserIdAndId(userId, taskId);
        task.setProjectId(projectId);
        taskRepository.save(task);
    }

    @Override
    @Transactional
    public void removeProjectById(@NotNull final String userId, @NotNull final String projectId) throws AbstractException {
        if (projectId.isEmpty())
            throw new IdEmptyException();
        if (!projectRepository.existsByUserIdAndId(userId, projectId))
            throw new ProjectNotFoundException();

        @NotNull final List<TaskDTO> tasks = taskRepository.findByUserIdAndProjectId(userId, projectId);
        for (TaskDTO task : tasks)
            taskRepository.deleteById(task.getId());
        projectRepository.deleteByUserIdAndId(userId, projectId);
    }

    @Override
    @Transactional
    public void unbindTaskFromProject(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) throws AbstractException {
        if (projectId.isEmpty())
            throw new IdEmptyException();
        if (taskId.isEmpty())
            throw new IdEmptyException();
        if (!projectRepository.existsByUserIdAndId(userId, projectId))
            throw new ProjectNotFoundException();

        @NotNull final TaskDTO task = taskRepository.findByUserIdAndId(userId, taskId);
        task.setProjectId(null);
        taskRepository.save(task);
    }

}
