package ru.mtumanov.tm.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("ru.mtumanov.tm")
public class ApplicationConfiguration {

}
