package ru.mtumanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.mtumanov.tm.dto.model.ProjectDTO;
import ru.mtumanov.tm.enumerated.Status;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

@Repository
public class ProjectRepository {

    @NotNull
    private static final ProjectRepository INSTANCE = new ProjectRepository();

    private static int i = 0;
    @NotNull
    private Map<String, ProjectDTO> projects = new LinkedHashMap<>();

    {
        save(new ProjectDTO("One", "One desc", Status.NOT_STARTED));
        save(new ProjectDTO("Two", "Two desc", Status.NOT_STARTED));
        save(new ProjectDTO("Three", "Three desc", Status.IN_PROGRESS));
        save(new ProjectDTO("Four", "Four desc", Status.IN_PROGRESS));
        save(new ProjectDTO("Five", "Five desc", Status.COMPLETED));
    }

    private ProjectRepository() {
    }

    @NotNull
    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    public void create() {
        save(new ProjectDTO("New Project " + i++, String.valueOf(System.currentTimeMillis()), Status.NOT_STARTED));
    }

    public void save(@NotNull final ProjectDTO project) {
        projects.put(project.getId(), project);
    }

    @Nullable
    public Collection<ProjectDTO> findAll() {
        return projects.values();
    }

    @Nullable
    public ProjectDTO findById(@NotNull final String id) {
        return projects.get(id);
    }

    public void removeById(@NotNull final String id) {
        projects.remove(id);
    }

}
