package ru.mtumanov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.task.TaskRemoveByIdRq;
import ru.mtumanov.tm.dto.response.task.TaskRemoveByIdRs;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

@Component
public class TaskRemoveByIdListener extends AbstractTaskListener {

    @Override
    @NotNull
    public String getDescription() {
        return "Remove task by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-remove-by-id";
    }

    @Override
    @EventListener(condition = "@taskRemoveByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final TaskRemoveByIdRq request = new TaskRemoveByIdRq(getToken(), id);
        @NotNull final TaskRemoveByIdRs response = getTaskEndpoint().taskRemoveById(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
