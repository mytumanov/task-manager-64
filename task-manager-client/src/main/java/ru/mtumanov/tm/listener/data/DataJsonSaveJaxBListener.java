package ru.mtumanov.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.data.DataJsonSaveJaxbRq;
import ru.mtumanov.tm.dto.response.data.DataJsonSaveJaxbRs;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.exception.AbstractException;

@Component
public class DataJsonSaveJaxBListener extends AbstractDataListener {

    @Override
    @NotNull
    public String getDescription() {
        return "Save data to json file";
    }

    @Override
    @NotNull
    public String getName() {
        return "data-save-json-jaxb";
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @EventListener(condition = "@dataJsonSaveJaxBListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[DATA SAVE JSON]");
        @NotNull final DataJsonSaveJaxbRs response = getDomainEndpoint().saveDataJsonJaxb(new DataJsonSaveJaxbRq(getToken()));
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
