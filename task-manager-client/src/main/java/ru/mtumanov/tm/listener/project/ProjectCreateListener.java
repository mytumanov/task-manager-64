package ru.mtumanov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.project.ProjectCreateRq;
import ru.mtumanov.tm.dto.response.project.ProjectCreateRs;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

@Component
public class ProjectCreateListener extends AbstractProjectListener {

    @Override
    @NotNull
    public String getDescription() {
        return "Create new project";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-create";
    }

    @Override
    @EventListener(condition = "@projectCreateListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectCreateRq request = new ProjectCreateRq(getToken(), name, description);
        @NotNull final ProjectCreateRs response = getProjectEndpoint().projectCreate(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
