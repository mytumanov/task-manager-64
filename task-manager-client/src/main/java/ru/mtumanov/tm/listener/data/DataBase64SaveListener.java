package ru.mtumanov.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.data.DataBase64SaveRq;
import ru.mtumanov.tm.dto.response.data.DataBase64SaveRs;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.exception.AbstractException;

@Component
public class DataBase64SaveListener extends AbstractDataListener {

    @NotNull
    private static final String NAME = "data-save-base64";

    @Override
    @NotNull
    public String getDescription() {
        return "Save data to base64 file.";
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

    @Override
    @EventListener(condition = "@dataBase64SaveListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[DATA SAVE BASE64]");
        @NotNull final DataBase64SaveRs response = getDomainEndpoint().saveDataBase64(new DataBase64SaveRq(getToken()));
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
